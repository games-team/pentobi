Source: pentobi
Section: games
Priority: optional
Maintainer: Debian Games Team <pkg-games-devel@lists.alioth.debian.org>
Uploaders: Juhani Numminen <juhaninumminen0@gmail.com>
Build-Depends: appstream,
               cmake,
               debhelper-compat (= 13),
               gettext,
               itstool,
               librsvg2-bin,
               qt6-base-dev (>= 6.4),
               qt6-declarative-dev,
               qt6-tools-dev,
               libxkbcommon-dev,
               qt6-declarative-private-dev,
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://pentobi.sourceforge.io
Vcs-Git: https://salsa.debian.org/games-team/pentobi.git
Vcs-Browser: https://salsa.debian.org/games-team/pentobi

Package: pentobi
Architecture: any
Depends: qml6-module-qtcore,
         qml6-module-qt-labs-settings,
         qml6-module-qtqml-workerscript,
         qml6-module-qtquick-controls,
         qml6-module-qtquick-dialogs,
         qml6-module-qtquick-layouts,
         qml6-module-qtquick-templates,
         qml6-module-qtquick-window,
         ${misc:Depends},
         ${shlibs:Depends}
Description: clone of the strategy board game Blokus
 Pentobi is a strategy board game played on a square grid board using playing
 pieces in the shapes of polyominoes, similar to Tetris pieces.  The aim of the
 game is to place all of your pieces while trying to block your opponent from
 placing all of theirs.
 .
 The game ends when no one can place any more pieces and the winner is
 determined by calculating the score based on the points of any remaining
 playing pieces. Bonus points are added for playing every piece and additional
 bonus points are added if the monomino, the 1x1 square piece, is played last.
