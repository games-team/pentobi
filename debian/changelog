pentobi (25.2-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 25.2
  * Update BD (Closes: #1088893)
  * Bump to SV 4.7.0

 -- Patrice Duroux <patrice.duroux@gmail.com>  Wed, 01 Jan 2025 13:06:08 +0100

pentobi (25.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 25.1

 -- Alexandre Detiste <tchet@debian.org>  Wed, 16 Oct 2024 12:28:03 +0200

pentobi (25.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 25.0

 -- Alexandre Detiste <tchet@debian.org>  Wed, 08 May 2024 23:38:46 +0200

pentobi (24.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 24.0
  * Sync dependencies according to INSTALL.md

 -- Alexandre Detiste <tchet@debian.org>  Thu, 04 Apr 2024 17:29:57 +0200

pentobi (23.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Update (build) dependencies.
  * Make sure Qt6 is selected for building.
  * Update paths in d/copyright.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 25 Jun 2023 13:55:02 +0200

pentobi (22.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Remove unused dependency on libqt5svg5. (Closes: #1029258)
  * Drop pentobi-kde-thumbnailer package, as support has been
    removed upstream.
  * Switch (build-)dependencies to Qt6.
  * Drop d/pentobi-install, as we install everything from upstream.
  * Update copyrights.

 -- Reiner Herrmann <reiner@reiner-h.de>  Wed, 01 Feb 2023 20:47:52 +0100

pentobi (21.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
  * Drop patch applied upstream.
  * d/control:
    - Drop qml-module-qtqml dependency (no longer required)
    - Drop build-dependency on qtwebview and qt5svg
    - Drop recommendation on www-browser (help is now always displayed
      in the applicationn)
    - Bump Standards-Version to 4.6.2
  * Add upstream metadata.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 08 Jan 2023 19:13:48 +0100

pentobi (18.3-2) unstable; urgency=medium

  * Team upload.
  * Declare compliance with Debian Policy 4.6.0.
  * Fix FTBFS with GCC 11. (Closes: #984290)

 -- Markus Koschany <apo@debian.org>  Fri, 22 Oct 2021 00:11:53 +0200

pentobi (18.3-1) unstable; urgency=medium

  * New upstream release.
    - Fixes compilation with Qt 5.15 (Closes: #975128).
  * d/control:
    - Use debhelper compatibility level v13.
    - Remove Dean Evans from uploaders because of long inactivity.
      Thank you for your contribution as the initial maintainer!
    - Bump Standards-Version to 4.5.1. No changes needed.
    - Add libqt5svg5 to Depends, required for SVG images (Closes: #975314).
    - Depend on qml-module-qtqml just to avoid issues with Qt 5.14 and earlier
      (Closes: #973855).
  * d/copyright:
    - Update years.
    - Change Upstream-Contact from the recently closed mailing list to
      GitHub issue tracker.
  * d/rules: Drop the custom linker flag -Wl,--as-needed which is not
    necessary anymore after becoming the default.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Fri, 20 Nov 2020 17:04:16 +0200

pentobi (17.3-1) unstable; urgency=medium

  * New upstream release.
  * d/rules: Build system now requires us to specify a cmake flag on
    architectures that do not have Qt WebView.
  * Bump Standards-Version to 4.4.1. No changes needed.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Sun, 15 Dec 2019 16:33:54 +0200

pentobi (17.1-1) unstable; urgency=medium

  * New upstream release. Upload to unstable as the freeze is over.
  * Add new build-dependencies: appstream, docbook-xsl, gettext, itstool,
    librsvg2-bin, xsltproc.
  * d/rules: PENTOBI_BUILD_TESTS was renamed to BUILD_TESTING.
  * d/*.install: thumbnailer metainfo file was removed upstream.
  * d/pentobi.docs: NEWS was renamed to NEWS.md.
  * Bump Standards-Version to 4.4.0. No changes needed.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Sun, 07 Jul 2019 20:41:44 +0300

pentobi (16.3-1) experimental; urgency=medium

  * New upstream release.
    - QtQuickCompiler was disabled to avoid the need for recompilation
      for different versions of Qt (LP: #1824560).

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Tue, 23 Apr 2019 11:14:26 +0300

pentobi (16.2-1) unstable; urgency=medium

  * New upstream release.
  * This version makes Qt WebView dependency optional.
    - libqt5webview5-dev and qml-module-qtwebview restricted to architectures
      for which they are available.
    - On other architectures, a browser is used to show the help.
      There is no virtual package that guarantees a graphical browser,
      but Recommends: www-browser may be good enough.
  * d/pentobi.install: Added back usr/share/help.
  * Bump Standards-Version to 4.3.0. No changes needed.
  * Change to compat 12 by Build-Depends: debhelper-compat.
  * d/copyright: Update years.
  * d/copyright, d/watch: Change http to https.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Thu, 17 Jan 2019 16:42:18 +0200

pentobi (16.1-1) unstable; urgency=medium

  * New upstream release.
  * Declare compliance with Policy 4.2.1.
    - Do not rename NEWS to changelog; that has been deprecated.
  * Add new Build-Depends.
  * Add Depends on required qml-module-* packages.
  * Export QT_SELECT to avoid "could not find a Qt installation of ''".
  * The help files and game resources are now bundled into the executable.
    - d/pentobi.install: Removed usr/share/games and usr/share/help.
    - d/pentobi.doc-base.*: Removed.
  * d/pentobi-kde-thumbnailer.install: Update metainfo filename.
  * Re-add "Enhances: konqueror" for -kde-thumbnailer because that has
    become true again.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Fri, 23 Nov 2018 12:48:23 +0200

pentobi (14.1-1) unstable; urgency=medium

  * New upstream release.
  * Update copyright years.
  * Upgrade to dh compat level 11.
  * Drop alternative Build-Depends on kio-dev, which is an old name and
    not needed any more.
  * Update Vcs-Git and Vcs-Browser to salsa.debian.org.
  * Declare compliance with Policy 4.1.3. No changes needed.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Thu, 11 Jan 2018 21:53:32 +0200

pentobi (14.0-1) unstable; urgency=medium

  * New upstream release.
  * Bump Standards-Version to 4.1.1, no changes needed.
  * Add Rules-Requires-Root: no.
  * Make an 'empty' override target actually empty, so that dh does not
    call debian/rules unnecessarily.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Wed, 29 Nov 2017 09:31:38 +0200

pentobi (13.1-1) unstable; urgency=medium

  * New upstream release.
    - Update debian/*.install for new AppStream metainfo locations.
  * Bump Standards-Version to 4.0.0, no changes needed.
  * Update homepage address.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Tue, 04 Jul 2017 20:51:47 +0300

pentobi (12.2-1) unstable; urgency=medium

  * New upstream release.
  * d/control: Build-Dep kio-dev was renamed to libkf5kio-dev, but keep
    the old name as an alternative (for Ubuntu).
  * d/copyright:
    - Update copyright years.
    - Use https in the copyright-format URL.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Thu, 05 Jan 2017 22:14:45 +0200

pentobi (12.1-1) unstable; urgency=medium

  * New upstream release.
    - Contains d/patches/desktop-semicolon.patch. Remove d/patches.
  * Use dh compat 10. Option --parallel is now default, dropped.
  * Mark pentobi-kde-thumbnailer Multi-Arch same, as hinted by MA-hinter.
  * Update watch file to version 4.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Thu, 01 Dec 2016 16:14:42 +0200

pentobi (12.0-1) unstable; urgency=medium

  * New upstream release.
  * d/control:
    - Bump Standards-Version to 3.9.8, no changes needed.
    - Unify Vcs-Git and Vcs-Browser.
    - Konqueror is not KDE Frameworks 5 application and thus cannot use
      pentobi-kde-thumbnailer; removed Enhances.
  * d/copyright: Update copyright years.
  * d/patches/desktop-semicolon.patch: Add a missing ; in desktop file.
  * d/rules: Enable all hardening flags.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Tue, 17 May 2016 21:41:19 +0300

pentobi (11.0-1) unstable; urgency=medium

  * New upstream release.
  * d/rules: "USE_QT5" not needed, qt4 support was dropped upstream.
  * d/pentobi.install: Add usr/share/appdata, remove usr/share/pixmaps.
  * d/control: Update Vcs-Browser field to use https.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Sat, 02 Jan 2016 19:40:55 +0200

pentobi (10.1-1) unstable; urgency=medium

  * New upstream release.
  * Drop debian/pentobi.menu as per tech-ctte decision in #741573.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Thu, 24 Dec 2015 18:14:00 +0200

pentobi (10.0-1) unstable; urgency=medium

  * New upstream release.
  * Build with Qt5.
    - d/rules: Add Qt5 cmake flags.
    - d/control: Adjust Build-Deps.
    - d/pentobi-kde-thumbnailer.install: Adjust for new install locations.
  * Drop now unneeded override_dh_install.
  * d/pentobi.install, d/pentobi.doc-base.*: Adjust for new help path.
  * d/pentobi.doc-base.manual-en-{ca,gb}: en_CA, en_GB dropped upstream.
  * d/copyright: Update copyright years.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Sun, 05 Jul 2015 19:37:05 +0300

pentobi (9.0-2) unstable; urgency=medium

  * Upload to unstable.
  * Reduce size of d/upstream/signing-key.asc by using a minimally exported key.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Thu, 30 Apr 2015 18:39:00 +0300

pentobi (9.0-1) experimental; urgency=medium

  * New upstream release. Upload to experimental.
  * Restructure debian/rules.
  * Do not find and symlink duplicate files, not needed anymore.
  * Bump Standards-Version to 3.9.6, no changes needed.
  * Do not install README, which has no useful information.
  * Support noopt in DEB_BUILD_OPTIONS.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Sun, 14 Dec 2014 13:52:01 +0200

pentobi (8.2-1) unstable; urgency=medium

  * New upstream release.
    - Fixes link error on some platforms if Pentobi is compiled with
      PENTOBI_BUILD_TESTS (Closes: #759852).
  * Update Vcs-Browser field.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Sat, 06 Sep 2014 09:23:17 +0300

pentobi (8.0-2) unstable; urgency=medium

  * Remove export CC/CXX lines and Build-Depends on g++-4.8 since the default
    compiler is g++-4.9 on release architectures (Closes: #751326).

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Sun, 15 Jun 2014 11:51:50 +0300

pentobi (8.0-1) unstable; urgency=medium

  * New upstream release.
  * Use more flexible file extension regex in debian/watch.
  * Symlink duplicates in the whole pentobi binary package.
  * Don’t rebuild images in d/rules, upstream build system now does it.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Thu, 06 Mar 2014 09:22:33 +0200

pentobi (7.2-1) unstable; urgency=medium

  * New upstream release.
  * Import upstream GPG key for uscan to verify the orig tarball.
  * Build-Depend on g++-4.8 instead of g++ (>= 4:4.7) and export
    CXX=g++-4.8 to be able to build on arches with older default gcc.
  * Update copyright years.
  * Drop all patches; they have been applied upstream.
    - 0001-hyphen-used-as-minus-sign.patch
    - 0002-desktop-entry-keywords.patch
    - 0003-fix-RandomGenerator-compilation-error.patch

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Thu, 30 Jan 2014 20:25:26 +0200

pentobi (7.1-2) unstable; urgency=low

  * Bump Standards-Version to 3.9.5, no changes needed.
  * Add a patch to fix RandomGenerator compilation error.
  * Build-Depend on g++ >= 4:4.7 to avoid build failures caused by
    -std=c++11 option being unknown.
  * Install the file ’NEWS’ as ’changelog’.
  * Enable KDE thumbnailer.
  * Build a separate binary package for the KDE thumbnailer to avoid
    dependency on KDE libraries.
  * Shorten the too long short description.
  * Wrap and sort packaging files.
  * Generate PNG images from their SVG sources.
  * Remove workaround for cmake ignoring CPPFLAGS which isn’t needed any more.
  * Compile with -O3 to make the computationally expensive higher levels faster.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Sat, 14 Dec 2013 20:26:55 +0200

pentobi (7.1-1) unstable; urgency=low

  * New upstream release.
  * Add myself to Uploaders.
  * Bump Standards-Version to 3.9.4, no changes needed.
  * Update debian/copyright.
    + Add Upstream-Contact field.
    + Update copyright years.
    + Remove the Tango icons which Pentobi upstream is no longer shipping.
    + Add myself to packaging files’ copyright holders.
  * Fix path for the svg and generated xpm icons.
  * Improve debian/menu longtitle.
    Thanks to Bill Allombert, Chris Leick (Closes: #684476)
  * Don’t install upstream changelog twice.
  * Remove unneeded Build-Depends on boost libraries and Depends on libqt4-svg.
  * Use canonical Vcs-Git and Vcs-Browser URIs.
  * Fix CMake flags.
  * Register the user manuals with doc-base.
  * Replace duplicate files of en_CA and en_GB manuals with symlinks.
  * Unapply patches from source to ease package maintaining with git.
  * Add patches.
    + hyphen-used-as-minus-sign to fix manpage using hyphens as minus signs
    + desktop-entry-keywords to add Keywords to desktop entry
  * Remove installed COPYING file containing GPL-3.0+.

 -- Juhani Numminen <juhaninumminen0@gmail.com>  Sun, 25 Aug 2013 10:37:54 +0300

pentobi (1.1-1) unstable; urgency=low

  * New upstream release.
  * Fix path for the moved svg image and consequently the install
    path for the converted xpm image.
  * Bump compat to 9 for hardening flags.
  * Add workaround for cmake ignoring CPPFLAGS to enable hardening
    buildflags.
  * Build-Depend on debhelper v9.
  * Bump Standards-Version to 3.9.3 without change.
  * Add libqt4-svg to Depends.
  * Update copyright Format URL now that it is officially released.

 -- Dean Evans <dean@codemonkies.net>  Tue, 13 Mar 2012 23:59:11 +1300

pentobi (1.0-1) unstable; urgency=low

  * Initial release (Closes: #651789)

 -- Dean Evans <dean@codemonkies.net>  Sat, 10 Dec 2011 14:54:31 +1300
